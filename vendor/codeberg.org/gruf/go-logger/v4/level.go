package logger

import (
	"fmt"
	"strings"
)

// LEVEL defines a level of logging.
type LEVEL uint8

// Default levels of logging.
const (
	UNSET LEVEL = 0
	PANIC LEVEL = 50
	ERROR LEVEL = 100
	WARN  LEVEL = 150
	INFO  LEVEL = 200
	DEBUG LEVEL = 250
	TRACE LEVEL = 254
	ALL   LEVEL = ^LEVEL(0)
)

// CanLog returns whether an incoming log of 'lvl' can be logged against receiving level.
func (loglvl LEVEL) CanLog(lvl LEVEL) bool {
	return loglvl > lvl
}

// ParseLevel will attempt to decode a LEVEL from given string, checking (case insensitive) against strings in Levels.
func ParseLevel(s string) (LEVEL, error) {
	// Ensure consistent casing
	s = strings.ToUpper(s)

	for lvl := LEVEL(0); int(lvl) < len(levels); lvl++ {
		// Compare to eqach known level
		if strings.ToUpper(levels[lvl]) == s {
			return lvl, nil
		}
	}

	return 0, fmt.Errorf("unrecognized log level: %q", s)
}

var levels = [256]string{
	TRACE: "TRACE",
	DEBUG: "DEBUG",
	INFO:  "INFO",
	WARN:  "WARN",
	ERROR: "ERROR",
	PANIC: "PANIC",
}
