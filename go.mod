module codeberg.org/gruf/kvstored

go 1.19

replace codeberg.org/gruf/go-fflag => ../go-fflag

require (
	codeberg.org/gruf/go-bytesize v1.0.2
	codeberg.org/gruf/go-byteutil v1.1.2
	codeberg.org/gruf/go-ctx v1.0.2
	codeberg.org/gruf/go-debug v1.3.0
	codeberg.org/gruf/go-fastcopy v1.1.2
	codeberg.org/gruf/go-fflag v0.0.0-20230816221011-d314927eb0ce
	codeberg.org/gruf/go-fsys v0.0.0-20230508183124-d1809af32b35
	codeberg.org/gruf/go-iotools v0.0.0-20230811115124-5d4223615a7f
	codeberg.org/gruf/go-kv v1.6.4
	codeberg.org/gruf/go-logger/v4 v4.0.3
	codeberg.org/gruf/go-middleware v1.2.1
	codeberg.org/gruf/go-nowish v1.1.2
	codeberg.org/gruf/go-store/v2 v2.2.2
	codeberg.org/gruf/go-ulid v1.1.0
	github.com/julienschmidt/httprouter v1.3.0
)

require (
	codeberg.org/gruf/go-bitutil v1.1.0 // indirect
	codeberg.org/gruf/go-bytes v1.0.2 // indirect
	codeberg.org/gruf/go-errors/v2 v2.2.0 // indirect
	codeberg.org/gruf/go-fastpath v1.0.3 // indirect
	codeberg.org/gruf/go-fastpath/v2 v2.0.0 // indirect
	codeberg.org/gruf/go-hashenc v1.0.2 // indirect
	codeberg.org/gruf/go-mutexes v1.1.5 // indirect
	codeberg.org/gruf/go-pools v1.1.0 // indirect
	codeberg.org/gruf/go-split v1.1.0 // indirect
	github.com/cornelk/hashmap v1.0.8 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/felixge/httpsnoop v1.0.3 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/klauspost/compress v1.16.7 // indirect
	github.com/klauspost/cpuid/v2 v2.2.5 // indirect
	github.com/minio/md5-simd v1.1.2 // indirect
	github.com/minio/minio-go/v7 v7.0.62 // indirect
	github.com/minio/sha256-simd v1.0.1 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/rs/xid v1.5.0 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	golang.org/x/crypto v0.12.0 // indirect
	golang.org/x/exp v0.0.0-20230817173708-d852ddb80c63 // indirect
	golang.org/x/net v0.14.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
	golang.org/x/text v0.12.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)
