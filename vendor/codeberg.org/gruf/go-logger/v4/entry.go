package logger

import (
	"fmt"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-kv"
)

type Entry struct {
	Lvl LEVEL

	Data []kv.Field

	Msg string

	Out *Logger
}

func (e Entry) With(fields ...kv.Field) Entry {
	return Entry{
		Lvl:  e.Lvl,
		Data: append(e.Data, fields...),
		Msg:  e.Msg,
		Out:  e.Out,
	}
}

func (e Entry) Trace(args ...interface{}) {
	e.Lvl = TRACE
	e.Msg = fmt.Sprint(args...)
	e.Write(3)
}

func (e Entry) Tracef(msg string, args ...interface{}) {
	e.Lvl = TRACE
	e.Msg = fmt.Sprintf(msg, args...)
	e.Write(3)
}

func (e Entry) TraceKVs(fields ...kv.Field) {
	e.Lvl = TRACE
	e.Data = append(e.Data, fields...)
	e.Write(3)
}

func (e Entry) Debug(args ...interface{}) {
	e.Lvl = DEBUG
	e.Msg = fmt.Sprint(args...)
	e.Write(3)
}

func (e Entry) Debugf(msg string, args ...interface{}) {
	e.Lvl = DEBUG
	e.Msg = fmt.Sprintf(msg, args...)
	e.Write(3)
}

func (e Entry) DebugKVs(fields ...kv.Field) {
	e.Lvl = DEBUG
	e.Data = append(e.Data, fields...)
	e.Write(3)
}

func (e Entry) Info(args ...interface{}) {
	e.Lvl = INFO
	e.Msg = fmt.Sprint(args...)
	e.Write(3)
}

func (e Entry) Infof(msg string, args ...interface{}) {
	e.Lvl = INFO
	e.Msg = fmt.Sprintf(msg, args...)
	e.Write(3)
}

func (e Entry) InfoKVs(fields ...kv.Field) {
	e.Lvl = INFO
	e.Data = append(e.Data, fields...)
	e.Write(3)
}

func (e Entry) Warn(args ...interface{}) {
	e.Lvl = WARN
	e.Msg = fmt.Sprint(args...)
	e.Write(3)
}

func (e Entry) Warnf(msg string, args ...interface{}) {
	e.Lvl = WARN
	e.Msg = fmt.Sprintf(msg, args...)
	e.Write(3)
}

func (e Entry) WarnKVs(fields ...kv.Field) {
	e.Lvl = WARN
	e.Data = append(e.Data, fields...)
	e.Write(3)
}

func (e Entry) Error(args ...interface{}) {
	e.Lvl = ERROR
	e.Msg = fmt.Sprint(args...)
	e.Write(3)
}

func (e Entry) Errorf(msg string, args ...interface{}) {
	e.Lvl = ERROR
	e.Msg = fmt.Sprintf(msg, args...)
	e.Write(3)
}

func (e Entry) ErrorKVs(fields ...kv.Field) {
	e.Lvl = ERROR
	e.Data = append(e.Data, fields...)
	e.Write(3)
}

func (e Entry) Panic(args ...interface{}) {
	e.Lvl = PANIC
	e.Msg = fmt.Sprint(args...)
	defer panic(e.Msg)
	e.Write(3)
}

func (e Entry) Panicf(msg string, args ...interface{}) {
	e.Lvl = PANIC
	e.Msg = fmt.Sprintf(msg, args...)
	defer panic(e.Msg)
	e.Write(3)
}

func (e Entry) PanicKVs(fields ...kv.Field) {
	e.Lvl = PANIC
	e.Data = append(e.Data, fields...)
	defer panic(kv.Fields(fields).String())
	e.Write(3)
}

func (e Entry) Write(calldepth int) {
	e.Out.Write(calldepth+1, e.Lvl, func(buf *byteutil.Buffer) {
		if len(e.Data) > 0 {
			// Append formatted kv fields
			kv.Fields(e.Data).AppendFormat(buf, false)
			buf.B = append(buf.B, ' ')
		}
		if e.Msg != "" {
			// Append quoted message string
			buf.B = append(buf.B, `msg=`...)
			kv.AppendQuoteString(buf, e.Msg)
		}
	})
}
