#!/bin/sh

# Check being run at repository root
[ -x './scripts/build.sh' ] || {
    echo 'run script at repository root'
    exit 1
}

# Pass onto main build script
exec env \
DEBUG=$DEBUG \
BUILD_DIR='.' \
BUILD_PKG='.' \
BUILD_OUT='./kvstored' \
'./scripts/build.sh'