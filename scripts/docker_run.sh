#!/bin/sh

set -e

# Print message and exit
die() { echo "$*"; exit 1; }

# Check for root
[ $# -eq 2 ] || \
    die "Usage: ${0} <host_root> <env_file>"

# Check being run at repository root
[ -x './scripts/build.sh' ] || \
    die 'run script at repository root'

# Check for env file existence
[ -r "$2" ] || \
    die 'invalid .env file (does it exist?)'

# Load env into shell
eval "$(cat "${2}")"

# Drop all up-to port
PORT=${LISTEN#*:}
[ -z "$PORT" ] && \
    die 'invalid $LISTEN address'

# Build the image
echo 'Building image...'
IID=$(docker build -q .)

# Run the container!
echo 'Starting container...'
CID=$(docker run --rm --detach --expose "$PORT" \
                 --env-file "$2" \
                 --mount "type=bind,source=${1},target=/app/store" \
                 $IID)

# Print container information
echo -n "Started container: ${CID}\nIP: "
docker container inspect --format '{{ .NetworkSettings.IPAddress }}' $CID