package main

import (
	"sync"

	"codeberg.org/gruf/go-ctx"
)

// chanPool is a pool of simple struct{} channels
// useful when sychronizing multiple threads.
var chanPool = sync.Pool{
	New: func() interface{} {
		return make(chan struct{})
	},
}

// ctxPool is a pool of ctx.Context objects for use
// in the encompassing state structures.
var ctxPool = sync.Pool{
	New: func() interface{} {
		return ctx.New()
	},
}
