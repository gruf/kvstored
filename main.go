package main

import (
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"codeberg.org/gruf/go-bytesize"
	"codeberg.org/gruf/go-debug"
	"codeberg.org/gruf/go-fflag"
	"codeberg.org/gruf/go-fsys"
	"codeberg.org/gruf/go-kv"
	"codeberg.org/gruf/go-logger/v4"
	"codeberg.org/gruf/go-logger/v4/httplog"
	"codeberg.org/gruf/go-middleware"
	"codeberg.org/gruf/go-nowish"
	kvstore "codeberg.org/gruf/go-store/v2/kv"
	"codeberg.org/gruf/go-store/v2/storage"
	"github.com/julienschmidt/httprouter"
)

var (
	// out is a stdout filehandle.
	out = fsys.Stdout()

	// clock is the global clock used for logging.
	clock, _ = nowish.Start(time.Second)

	// log is the global logger instance.
	log = logger.New(
		logger.WithSafeOutput(out),
		logger.WithTimestamp(true),
		logger.WithCaller(false),
		logger.WithLevel(logger.ALL),
		logger.WithClock(clock.NowFormat),
	)

	// httputil is the global HTTP middleware instance.
	httputil = middleware.New((*httplog.Logger)(log))
)

// config defines server CLI flag information with unmarshaling destinations.
var config = struct {
	Listen    string        `short:"l" long:"listen" env:"LISTEN" usage:"Server listen address" default:"127.0.0.1:8080"`
	TLSCert   string        `short:"c" long:"tls-cert" env:"TLS_CERT" usage:"Server TLS cert file" default:""`
	TLSKey    string        `short:"k" long:"tls-key" env:"TLS_KEY" usage:"Server TLS key file" default:""`
	Store     string        `short:"p" long:"store-path" env:"STORE_PATH" usage:"Server storage path" default:":memory:"`
	Compress  bool          `short:"z" long:"compress" env:"COMPRESS" usage:"Enable store compression (using snappy)" default:"false"`
	Timeout   time.Duration `short:"t" long:"timeout" env:"TIMEOUT" usage:"Store default timeout duration" default:"30s"`
	Overwrite bool          `short:"o" long:"overwrite" env:"OVERWRITE" usage:"Enable store overwrites" default:"false"`
	ReadBuf   bytesize.Size `short:"r" long:"read-buffer" env:"READ_BUFFER" usage:"Store read buffer size" default:"4096B"`
	WriteBuf  bytesize.Size `short:"w" long:"write-buffer" env:"WRITE_BUFFER" usage:"Store write buffer size" default:"4096B"`
}{}

func main() {
	var (
		app   App
		err   error
		store storage.Storage
	)

	// Set log time format.
	clock.SetFormat(`"2006/01/02 15:04:05"`)

	// Register CLI flags.
	fflag.StructFields(&config)
	fflag.Func("h", "help", "Print usage information", func() {
		out.WriteString("Usage: " + os.Args[0] + " ...\n" + fflag.Usage())
		syscall.Exit(0)
	})
	fflag.Func("v", "version", "Print version information", func() {
		out.WriteString(debug.BuildInfo("kvstored"))
		syscall.Exit(0)
	})

	// Parse CLI flags into config.
	if extra := fflag.MustParse(); len(extra) > 0 {
		out.WriteString("" +
			"Usage: " + os.Args[0] + " ...\n" +
			fflag.Usage() + "\n" +
			"unexpected cli args: " + strings.Join(extra, " ") + "\n",
		)
		syscall.Exit(1)
	}

	if config.Store == ":memory:" {
		// Open the in-memory storage
		log.Info("using in-memory storage")
		store = storage.OpenMemory(-1, config.Overwrite)
	} else {
		// Prepare the disk store config
		scfg := *storage.DefaultDiskConfig
		scfg.Overwrite = config.Overwrite
		scfg.WriteBufSize = int(config.WriteBuf)

		if config.Compress {
			// Setup snappy compression if requested
			scfg.Compression = storage.SnappyCompressor()
		}

		// All else are filesystem paths, open disk
		log.Info("opening disk storage: " + config.Store)
		disk, err := storage.OpenDisk(config.Store, &scfg)
		if err != nil {
			log.Panic(err)
		}

		// Set disk store
		store = disk
	}

	// Open KVStore from prepared storage
	log.Info("opening key-value store")
	kvstore, err := kvstore.OpenStorage(store)
	if err != nil {
		log.Panic(err)
	}
	defer kvstore.Close()

	// Setup the application
	router := httprouter.New()
	app.store = kvstore
	app.states = make(map[string]*State)
	app.unlocks = make(map[string]func())
	app.copy.Buffer(int(config.ReadBuf))

	// Perform .Get(), .Has(), .Put(), .Delete()
	router.GET("/kv/:key", app.Get)
	router.HEAD("/kv/:key", app.Has)
	router.POST("/kv/:key", app.Put)
	router.DELETE("/kv/:key", app.Delete)

	// Perform .Lock(), .RLock()
	router.POST("/kv/:key/rlock", app.RLock)
	router.POST("/kv/:key/lock", app.Lock)
	router.POST("/kv/:key/unlock", app.Unlock)

	// Perform .Read(), .Update() and .Release()
	router.POST("/st/read", app.Read)
	router.POST("/st/update", app.Update)
	router.POST("/st/release", app.Release)

	// Prepare the handler with middleware
	// (our app is DESIGNED around these middleware)
	handler := (http.Handler)(router)
	handler = debug.WithPprof(handler)
	handler = httputil.Recovery(handler)
	handler = httputil.Timeout(handler, app.timeout)
	handler = httputil.RequestID(handler)

	// Setup the HTTP server
	srv := http.Server{
		Addr:    config.Listen,
		Handler: handler,
	}

	// Log server configuration
	log.InfoKVs(kv.Field{
		K: "config",
		V: config,
	})

	// Listen for OS signals
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		log.Infof(`received signal %v, shutting down...`, <-sigs)
		_ = srv.Close() // shutdown server
	}()

	switch {
	// TLS cert and key file provided, use HTTPS
	case config.TLSCert != "" && config.TLSKey != "":
		log.Infof("listening on https://%s", config.Listen)
		err = srv.ListenAndServeTLS(config.TLSCert, config.TLSKey)

	// Use regular HTTP
	default:
		log.Infof("listening on: http://%s", config.Listen)
		err = srv.ListenAndServe()
	}

	// Log all errors (except server closed)
	if err != nil && err != http.ErrServerClosed {
		log.Panicf("error listening on HTTP server: %v", err)
	}
}
