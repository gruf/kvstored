package logger

import (
	"runtime"
	"strings"
	"sync"

	"codeberg.org/gruf/go-byteutil"
)

// Caller fetches the calling function name, skipping given frame count.
func Caller(skip int) string {
	var pcs [1]uintptr

	// Fetch calling function using calldepth
	_ = runtime.Callers(skip, pcs[:])
	fn := runtime.FuncForPC(pcs[0])

	if fn == nil {
		return ""
	}

	// Get func name for formatting.
	name := fn.Name()

	// Drop all but the package name and function name, no mod path
	if idx := strings.LastIndex(name, "/"); idx >= 0 {
		name = name[idx+1:]
	}

	const params = `[...]`

	// Drop any generic type parameter markers
	if idx := strings.Index(name, params); idx >= 0 {
		name = name[:idx] + name[idx+len(params):]
	}

	return name
}

// bufpool provides a memory  pool of byte buffers.
var bufpool = sync.Pool{
	New: func() interface{} {
		return &byteutil.Buffer{B: make([]byte, 0, 512)}
	},
}

// getBuf fetches a byte buffer from memory pool.
func getBuf() *byteutil.Buffer {
	return bufpool.Get().(*byteutil.Buffer)
}

// putBuf resets and places buffer back in memory pool.
func putBuf(buf *byteutil.Buffer) {
	if buf.Cap() > int(^uint16(0)) {
		return // drop large buffers
	}
	buf.Reset()
	bufpool.Put(buf)
}
