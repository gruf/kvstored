package main

import (
	"net/http"

	"codeberg.org/gruf/go-middleware"
)

// timedOut returns whether ResponseWriter has timed out.
func timedOut(rw http.ResponseWriter) bool {
	return rw.(*middleware.TimeoutResponseWriter).TimedOut()
}
