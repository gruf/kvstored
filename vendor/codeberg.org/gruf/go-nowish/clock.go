package nowish

import (
	"sync"
	"sync/atomic"
	"time"
	"unsafe"
)

// Start returns a new Clock instance initialized and
// started with the provided precision, along with the
// stop function for it's underlying timer.
func Start(precision time.Duration) (*Clock, func()) {
	c := Clock{}
	return &c, c.Start(precision)
}

// Clock provides a clock capable of returning current
// times up to a certain level of precision, far faster
// than using time.Now(). It does this by performing this
// regularly and caching the result, same for the formatted
// time string. All operations are atomic and protected
// by mutexes where necessary :).
type Clock struct {
	format string         // time format configuration
	mutex  sync.Mutex     // protects 'format' + multiple attempted formattings
	nowfmt unsafe.Pointer // ptr to last valid time format string (or empty)
	now    unsafe.Pointer // ptr to current time.Time
}

// Start starts the clock with the provided precision, the returned
// function is the stop function for the underlying timer. For >= 2ms,
// actual precision is usually within AT LEAST 10% of requested precision,
// less than this and the actual precision very quickly deteriorates.
func (c *Clock) Start(precision time.Duration) func() {
	// Create ticker from duration
	tick := time.NewTicker(precision / 10)

	// Set initial time
	t := time.Now()
	atomic.StorePointer(&c.now, unsafe.Pointer(&t))

	// Set initial format
	s := ""
	atomic.StorePointer(&c.nowfmt, unsafe.Pointer(&s))

	// If formatting string unset, set default
	c.mutex.Lock()
	if c.format == "" {
		c.format = time.RFC822
	}
	c.mutex.Unlock()

	// Start main routine
	go c.run(tick)

	// Return stop fn
	return tick.Stop
}

// run is the internal clock ticking loop.
func (c *Clock) run(tick *time.Ticker) {
	for {
		// Wait on tick
		_, ok := <-tick.C

		// Channel closed
		if !ok {
			break
		}

		// Update time
		t := time.Now()
		atomic.StorePointer(&c.now, unsafe.Pointer(&t))

		// Invalidate format string
		atomic.StorePointer(&c.nowfmt, nil)
	}
}

// Now returns a good (ish) estimate of the current 'now' time.
func (c *Clock) Now() time.Time {
	return *(*time.Time)(atomic.LoadPointer(&c.now))
}

// NowFormat returns the formatted "now" time, cached until next tick and "now" updates.
func (c *Clock) NowFormat() string {
	// If format still valid, return this
	if ptr := atomic.LoadPointer(&c.nowfmt); ptr != nil {
		return *(*string)(ptr)
	}

	// Get mutex lock
	c.mutex.Lock()

	// Double check still invalid
	if ptr := atomic.LoadPointer(&c.nowfmt); ptr != nil {
		c.mutex.Unlock()
		return *(*string)(ptr)
	}

	// Calculate time format + store
	nowfmt := c.Now().Format(c.format)
	atomic.StorePointer(&c.nowfmt, unsafe.Pointer(&nowfmt))

	// Unlock and return
	c.mutex.Unlock()
	return nowfmt
}

// SetFormat sets the time format string used by .NowFormat().
func (c *Clock) SetFormat(format string) {
	// Get mutex lock
	c.mutex.Lock()

	// Update time format
	c.format = format

	// Invalidate current format string
	atomic.StorePointer(&c.nowfmt, nil)

	// Unlock
	c.mutex.Unlock()
}
