package main

import (
	"context"
	"errors"
	"io"
	"net/http"
	"sync"
	"time"

	"codeberg.org/gruf/go-ctx"
	"codeberg.org/gruf/go-iotools"
	"codeberg.org/gruf/go-store/v2/kv"
)

// errNotWriteState is returned when a write is attempted on read-state.
var errNotWriteState = errors.New("state does not support write")

type State struct {
	tout  time.Duration   // tout is the timeout duration for this State
	state interface{}     // state contains kv.State{RO,RW}
	ctx   *ctx.Context    // ctx is our underlying ctx with deadline, enforcing a timeout
	done  <-chan struct{} // done is our own ptr to 'done' in ctx (useful so ctx can be recycled)
	cncl  func()          // cncl is a cancel function for our 'done' channel
	wait  sync.WaitGroup
}

// openState is the shared state opening functions handling all but the state opening itself.
func openState(r *http.Request, timeout time.Duration, open func() interface{}) (*State, bool) {
	// Prepare the context with cancel
	state := &State{ctx: ctxPool.Get().(*ctx.Context)}
	state.cncl = state.ctx.WithCancel()
	state.done = state.ctx.Done() // use our own ptr

	// Track when we can return (MUST DRAIN)
	opened := chanPool.Get().(chan struct{})
	defer chanPool.Put(opened)

	go func() {
		// Acquire the state
		state.state = open()
		opened <- struct{}{}

		// Wait on .Close()
		<-state.done

		// Wait for all uses done
		state.wait.Wait()

		// Release the state
		state.state.(interface {
			Release()
		}).Release()

		// Remove state ctx
		ctx := state.ctx
		state.ctx = nil

		// Release and unlock
		ctx.Reset()
		ctxPool.Put(ctx)
	}()

	select {
	// Read opened, set timeout
	case <-opened:
		state.ctx.WithTimeout(timeout)
		return state, true

	// Request timed out, release
	case <-r.Context().Done():
		<-opened // drain
		state.cncl()
		return nil, false
	}
}

// acquire will attempt to acquire access to this state,
// returning a state release function on success.
func (s *State) acquire() (release func(), ok bool) {
	select {
	// Already cancelled
	case <-s.done:
		return nil, false

	// Extend timeout
	default:
		s.ctx.WithTimeout(s.tout)
		return s.wait.Done, true
	}
}

func (s *State) GetStream(ctx context.Context, key string) (io.ReadCloser, error) {
	// Attempt to acquire access
	release, ok := s.acquire()
	if !ok {
		return nil, kv.ErrStateClosed
	}

	// Cast the state with method
	state := s.state.(interface {
		GetStream(context.Context, string) (io.ReadCloser, error)
	})

	// Attempt to get stream for key
	rc, err := state.GetStream(ctx, key)
	if err != nil {
		release() // done
		return nil, err
	}

	// Wrap the read closer to release state
	return iotools.ReadCloser(rc, iotools.CloserFunc(func() error {
		defer release()
		return rc.Close()
	})), nil
}

func (s *State) PutStream(ctx context.Context, key string, r io.Reader) (int64, error) {
	// Attempt to acquire access
	release, ok := s.acquire()
	if !ok {
		return 0, kv.ErrStateClosed
	}
	defer release()

	// Cast the state with method
	state, ok := s.state.(interface {
		PutStream(context.Context, string, io.Reader) (int64, error)
	})
	if !ok {
		return 0, errNotWriteState
	}

	// Place the value in the store
	return state.PutStream(ctx, key, r)
}

func (s *State) Has(ctx context.Context, key string) (bool, error) {
	// Attempt to acquire access
	release, ok := s.acquire()
	if !ok {
		return false, kv.ErrStateClosed
	}
	defer release()

	// Cast the state with method
	state := s.state.(interface {
		Has(context.Context, string) (bool, error)
	})

	// Check for value in store
	return state.Has(ctx, key)
}

func (s *State) Delete(ctx context.Context, key string) error {
	// Attempt to acquire access
	release, ok := s.acquire()
	if !ok {
		return kv.ErrStateClosed
	}
	defer release()

	// Cast the state with method
	state, ok := s.state.(interface {
		Delete(context.Context, string) error
	})
	if !ok {
		return errNotWriteState
	}

	// Delete the value from store
	return state.Delete(ctx, key)
}

// Closed will return if State is closed.
func (s *State) Closed() bool {
	select {
	case <-s.done:
		return true
	default:
		return false
	}
}

// Close will close this store state.
func (s *State) Close() {
	s.cncl()
}
