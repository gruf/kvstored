package main

import (
	"context"
	"errors"
	"io"
	"net/http"
	"sync"
	"time"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-fastcopy"
	"codeberg.org/gruf/go-store/v2/kv"
	"codeberg.org/gruf/go-store/v2/storage"
	"codeberg.org/gruf/go-ulid"
	"github.com/julienschmidt/httprouter"
)

type App struct {
	// underlying kv store
	store *kv.KVStore

	// state session -> store state
	states map[string]*State
	stmu   sync.Mutex

	// lock session -> key unlock funcs
	unlocks map[string]func()
	ulmu    sync.Mutex

	// state+req timeout duration
	timeout time.Duration

	// copy is the buffer pool backed request reader
	copy fastcopy.CopyPool
}

func (app *App) Get(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Configured store
	var store interface {
		GetStream(context.Context, string) (io.ReadCloser, error)
	}

	// Check for a supplied session key header
	session := r.Header.Get("Session")
	if session != "" {
		// A lock state  session key was provided
		state, ok := app.getState(session)
		if !ok {
			http.Error(rw, "Session Does Not Exist", http.StatusUnauthorized)
			return
		}

		store = state
	} else {
		// No session key
		store = app.store
	}

	// Fetch a reader for key in store
	rc, err := store.GetStream(r.Context(), p[0].Value)
	if err != nil {
		// Ignore timeouts
		if timedOut(rw) {
			return
		}

		// Catch and handle storage not found
		if errors.Is(err, storage.ErrNotFound) {
			http.Error(rw, "Key Not Found", http.StatusNotFound)
			return
		}

		// Catch and handle invalid keys
		if errors.Is(err, storage.ErrInvalidKey) {
			http.Error(rw, "Invalid Key", http.StatusBadRequest)
			return
		}

		// Catch and handle state closed
		if errors.Is(err, kv.ErrStateClosed) {
			http.Error(rw, "Session No Longer Valid", http.StatusUnauthorized)
			return
		}

		// Return a generic error message and log all other errors
		httputil.HTTPError(rw, r, "Internal Server Error", http.StatusInternalServerError, err)
		return
	}
	defer rc.Close()

	// Respond 'ok' with binary content-type
	rw.WriteHeader(200)
	rw.Header().Set("Content-Type", "application/octet-stream")

	// Write the resulting value to the client
	if _, err := app.copy.Copy(rw, rc); err != nil {
		httputil.HTTPError(rw, r, "Internal Server Error", http.StatusInternalServerError, err)
		return
	}
}

func (app *App) Put(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Configured store
	var store interface {
		PutStream(context.Context, string, io.Reader) (int64, error)
	}

	// Check for a supplied session key
	session := r.Header.Get("Session")
	if session != "" {
		// A lock state session key was provided
		state, ok := app.getState(session)
		if !ok {
			http.Error(rw, "Session Does Not Exist", http.StatusUnauthorized)
			return
		}

		store = state
	} else {
		// No session key
		store = app.store
	}

	// Attempt to place the key with body writer in store
	if _, err := store.PutStream(r.Context(), p[0].Value, r.Body); err != nil {
		// Ignore timeouts
		if timedOut(rw) {
			return
		}

		// Catch and handle existing keys
		if errors.Is(err, storage.ErrAlreadyExists) {
			http.Error(rw, "Key Already Exists", http.StatusConflict)
			return
		}

		// Catch and handle invalid keys
		if errors.Is(err, storage.ErrInvalidKey) {
			http.Error(rw, "Invalid Key", http.StatusBadRequest)
			return
		}

		// Catch and handle state closed
		if errors.Is(err, kv.ErrStateClosed) {
			http.Error(rw, "Session No Longer Valid", http.StatusUnauthorized)
			return
		}

		// Catch and handle state not supporting write
		if errors.Is(err, errNotWriteState) {
			http.Error(rw, "Session State Read Only", http.StatusUnauthorized)
			return
		}

		// Return a generic error message and log all other errors
		httputil.HTTPError(rw, r, "Internal Server Error", http.StatusInternalServerError, err)
		return
	}

	// Return with 'ok'
	rw.WriteHeader(200)
}

func (app *App) Has(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Configured store
	var store interface {
		Has(context.Context, string) (bool, error)
	}

	// Check for a supplied session key
	session := r.Header.Get("Session")
	if session != "" {
		// A lock state  session key was provided
		state, ok := app.getState(session)
		if !ok {
			http.Error(rw, "Session Does Not Exist", http.StatusUnauthorized)
			return
		}

		store = state
	} else {
		// No session key
		store = app.store
	}

	// Look in store for the key
	has, err := store.Has(r.Context(), p[0].Value)
	if err != nil {
		// Ignore timeouts
		if timedOut(rw) {
			return
		}

		// Catch and handle invalid keys
		if errors.Is(err, storage.ErrInvalidKey) {
			http.Error(rw, "Invalid Key", http.StatusBadRequest)
			return
		}

		// Catch and handle state closed
		if errors.Is(err, kv.ErrStateClosed) {
			http.Error(rw, "Session No Longer Valid", http.StatusUnauthorized)
			return
		}

		// Return a generic error message and log all other errors
		httputil.HTTPError(rw, r, "Internal Server Error", http.StatusInternalServerError, err)
		return
	} else if !has {
		// Key was unable to be found in store
		http.Error(rw, "Key Not Found", http.StatusNotFound)
		return
	}

	// Respond 'ok'
	rw.WriteHeader(200)
}

func (app *App) Delete(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Configured store
	var store interface {
		Delete(context.Context, string) error
	}

	// Check for a supplied session key
	session := r.Header.Get("Session")
	if session != "" {
		// A lock state session key was provided
		state, ok := app.getState(session)
		if !ok {
			http.Error(rw, "Session Does Not Exist", http.StatusUnauthorized)
			return
		}

		store = state
	} else {
		// No session key
		store = app.store
	}

	// Attempt to delete the key from the store
	if err := store.Delete(r.Context(), p[0].Value); err != nil {
		// Ignore timeouts
		if timedOut(rw) {
			return
		}

		// Catch and handle storage not found
		if errors.Is(err, storage.ErrNotFound) {
			http.Error(rw, "Key Not Found", http.StatusNotFound)
			return
		}

		// Catch and handle invalid keys
		if errors.Is(err, storage.ErrInvalidKey) {
			http.Error(rw, "Invalid Key", http.StatusBadRequest)
			return
		}

		// Catch and handle state closed
		if errors.Is(err, kv.ErrStateClosed) {
			http.Error(rw, "Session No Longer Valid", http.StatusUnauthorized)
			return
		}

		// Catch and handle state not supporting write
		if errors.Is(err, errNotWriteState) {
			http.Error(rw, "Session State Read Only", http.StatusUnauthorized)
			return
		}

		// Return a generic error message and log all other errors
		httputil.HTTPError(rw, r, "Internal Server Error", http.StatusInternalServerError, err)
		return
	}

	// Return with 'ok'
	rw.WriteHeader(200)
}

func (app *App) RLock(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Open RLock for given key
	runlock := app.store.RLock(p[0].Value)
	if timedOut(rw) {
		runlock()
		return
	}

	// Put lock in cache, getting key
	session := app.putUnlock(runlock)

	// Respond with new session key
	rw.WriteHeader(200)
	rw.Write(byteutil.S2B(session))
}

func (app *App) Lock(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Open Lock for given key
	unlock := app.store.Lock(p[0].Value)
	if timedOut(rw) {
		unlock()
		return
	}

	// Put lock in cache, getting key
	session := app.putUnlock(unlock)

	// Respond with new session key
	rw.WriteHeader(200)
	rw.Write(byteutil.S2B(session))
}

func (app *App) Unlock(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Check for a supplied session key
	session := r.Header.Get("Session")
	if session == "" {
		http.Error(rw, "No Session Provided", http.StatusBadRequest)
		return
	}

	// A lock session key was provided
	unlock, ok := app.getUnlock(session)
	if !ok {
		http.Error(rw, "Session Does Not Exist", http.StatusUnauthorized)
		return
	}

	// Unlock key, respond
	unlock()
	rw.WriteHeader(200)
}

func (app *App) Read(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Attempt to open read-only state using timeout
	state, ok := openState(r, app.timeout, func() interface{} {
		return app.store.Read()
	})
	if !ok /* i.e. timed out */ {
		return
	}

	// Put state in cache, get session key
	session, err := app.putState(state)
	if err != nil {
		httputil.HTTPError(rw, r, "Internal Server Error", http.StatusInternalServerError, err)
		return
	}

	// Respond with new session key
	rw.WriteHeader(200)
	rw.Write(byteutil.S2B(session))
}

func (app *App) Update(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Attempt to open read-write state using timeout
	state, ok := openState(r, app.timeout, func() interface{} {
		return app.store.Update()
	})
	if !ok /* i.e. timed out */ {
		return
	}

	// Put state in cache, get session key
	session, err := app.putState(state)
	if err != nil {
		httputil.HTTPError(rw, r, "Internal Server Error", http.StatusInternalServerError, err)
		return
	}

	// Respond with new session key
	rw.WriteHeader(200)
	rw.Write(byteutil.S2B(session))
}

func (app *App) Release(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Check for a supplied session key
	session := r.Header.Get("Session")
	if len(session) < 1 {
		http.Error(rw, "No Session Provided", http.StatusBadRequest)
		return
	}

	// A state session key was provided
	state, ok := app.getState(session)
	if !ok {
		http.Error(rw, "Session Does Not Exist", http.StatusUnauthorized)
		return
	}

	// Close the state, respond
	state.Close()
	rw.WriteHeader(200)
}

// getState safely checks for and returns store state in our states map.
func (app *App) getState(key string) (*State, bool) {
	// Safley check for state
	app.stmu.Lock()
	state, ok := app.states[key]

	// Check if found
	if !ok {
		app.stmu.Unlock()
		return nil, false
	}

	// Remove stale states
	if ok && state.Closed() {
		delete(app.states, key)
	}

	// Unlock and return
	app.stmu.Unlock()
	return state, true
}

// putState safely puts state in store and returns new session key for it.
func (app *App) putState(state *State) (string, error) {
	// Generate new UUID
	ulid, err := ulid.New()
	if err != nil {
		return "", err
	}

	// Create ULID key string
	key := ulid.String()

	// Place in states map
	app.stmu.Lock()
	app.states[key] = state
	app.stmu.Unlock()

	return key, nil
}

// getUnlock safely checks for and pops unlock func from our locks map.
func (app *App) getUnlock(key string) (func(), bool) {
	// Safely check for unlock
	app.ulmu.Lock()
	unlock, ok := app.unlocks[key]

	// Check if found
	if !ok {
		app.ulmu.Unlock()
		return nil, false
	}

	// Pop state from map
	delete(app.unlocks, key)

	// Unlock and return
	app.ulmu.Unlock()
	return unlock, true
}

// putUnlock safely puts unlock func in store and returns new session key for it.
func (app *App) putUnlock(unlock func()) string {
	// Create ULID key string
	key := ulid.MustNew().String()

	// Place in locks map
	app.ulmu.Lock()
	app.unlocks[key] = unlock
	app.ulmu.Unlock()

	return key
}
