package middleware

import (
	"sync/atomic"
)

// store will atomically store 2 uint32 vals into a uint64 ptr.
func store(ptr *uint64, v1, v2 uint32) {
	const mask = (1 << 32) - 1
	v := (uint64(v1) << 32) | (uint64(v2) & mask)
	atomic.StoreUint64(ptr, v)
}

// load will atomically load 2 uint32 vals from a uint64 ptr.
func load(ptr *uint64) (v1, v2 uint32) {
	const mask = (1 << 32) - 1
	v := atomic.LoadUint64(ptr)
	v1 = uint32(v >> 32)
	v2 = uint32(v & mask)
	return
}

// cas will perform a "double" atomic CAS operation on uint64 ptr storing 2 uint32 vals.
func cas(ptr *uint64, old1, old2 uint32, new1, new2 uint32) bool {
	const mask = (1 << 32) - 1
	old := (uint64(old1) << 32) | (uint64(old2) & mask)
	new := (uint64(new1) << 32) | (uint64(new2) & mask)
	return atomic.CompareAndSwapUint64(ptr, old, new)
}
